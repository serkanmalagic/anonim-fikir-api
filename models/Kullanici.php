<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

//VERİTABANINDAN GELEN VERİLERİ KLAS DEĞİŞKENLERİNE GÖMER VE DÖNDERİR

class Kullanici
{
  // DB stuff
  private $conn;
  private $table = 'kullanici';

  // Post Properties
  public $id;
  public $adi;
  public $mail;
  public $sifre;
  public $puan;
  public $created_at;


  // Constructor with DB
  public function __construct($db)
  {
    $this->conn = $db;
  }

  // Get Posts
  public function tum_kullanicilar()
  {
    // Create query
    $query = 'SELECT * FROM ' . $this->table;

    // Prepare statement
    $stmt = $this->conn->prepare($query);

    // Execute query
    $stmt->execute();

    return $stmt;
  }
  // Get Posts
  public function kullanici_kontrol()
  {
    // Create query
    $query = 'SELECT * FROM ' . $this->table . ' WHERE mail = ? ';

    // Prepare statement
    $stmt = $this->conn->prepare($query);

    // Bind ID
    $stmt->bindParam(1, $this->mail);

    // Execute query
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    // Set properties
    $this->id =      $row['id'];
    $this->adsoyad = $row['adsoyad'];
    $this->mail =    $row['mail'];
    $this->sifre =   $row['sifre'];
    $this->puan =     $row['puan'];
    $this->created_at = $row['created_at'];
  }

  // Create Post
  public function kullanici_kayit()
  {
    // Create query
    $query = 'INSERT INTO ' . $this->table . ' SET adsoyad = :adsoyad, mail = :mail, sifre = :sifre, puan = :puan';

    // Prepare statement
    $stmt = $this->conn->prepare($query);

    // Clean data
    $this->adsoyad = htmlspecialchars(strip_tags($this->adsoyad));
    $this->mail = htmlspecialchars(strip_tags($this->mail));
    $this->sifre = htmlspecialchars(strip_tags($this->sifre));
    $this->puan = htmlspecialchars(strip_tags($this->puan));

    // Bind data
    $stmt->bindParam(':adsoyad', $this->adsoyad);
    $stmt->bindParam(':mail', $this->mail);
    $stmt->bindParam(':sifre', $this->sifre);
    $stmt->bindParam(':puan', $this->puan);

    // Execute query
    if ($stmt->execute()) {
      return true;
    }

    // Print error if something goes wrong
    printf("Error: %s.\n", $stmt->error);

    return false;
  }

  public function kullanici_giris(){

    // Create query
    $query = 'SELECT * FROM ' . $this->table . ' WHERE mail = :mail AND sifre = :sifre ';

    // Prepare statement
    $stmt = $this->conn->prepare($query);

    // Bind ID
    $stmt->bindParam(":mail", $this->mail);
    $stmt->bindParam(":sifre", $this->sifre);

    // Execute query
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($row['id'] == null ){
      // giriş yapılan bilgiler doğru ise login yap user bilgilerini getir
      $this->id =      "boş";
      $this->adsoyad = "boş";
      $this->mail =    "boş";
      $this->sifre =   "boş";
      $this->puan =     "boş";
      $this->created_at = "boş";
    }
    else{
      // giriş yapılan bilgiler doğru ise login yap user bilgilerini getir
      $this->id =      $row['id'];
      $this->adsoyad = $row['adsoyad'];
      $this->mail =    $row['mail'];
      $this->sifre =   $row['sifre'];
      $this->puan =     (int)$row['puan'];
      $this->created_at = $row['created_at'];
    }
    


 }


 //BU KISIMDAN SONRA KULLANICIDAN AYRI OLARAK BULMA, PUAN EKLEME GİBİ İŞLEMLER YAPILACAKTIR.

 public function kullanici_bul(){

      // Create query
    $query = "SELECT * FROM " . $this->table . " WHERE adsoyad LIKE '%". $this->adsoyad ."%'";

    // Prepare statement
    $stmt = $this->conn->prepare($query);

    // Execute query
    $stmt->execute();

    return $stmt;

 }


}
