<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//VERİTABANINDAN GELEN VERİLERİ KLAS DEĞİŞKENLERİNE GÖMER VE DÖNDERİR
class Soru
{
  // DB stuff
  private $conn;
  private $table = 'sorular';

  // Post Properties
  public $id;
  public $soru;
  public $oylayan_sayisi;
  public $created_at;
  public $puan_tekil;


  // Constructor with DB
  public function __construct($db)
  {
    $this->conn = $db;
  }



  public function sorulari_getir(){
        // Create query
      $query = "SELECT * FROM sorular";

      // Prepare statement
      $stmt = $this->conn->prepare($query);

      // Execute query
      $stmt->execute();

      return $stmt;

  }


}
