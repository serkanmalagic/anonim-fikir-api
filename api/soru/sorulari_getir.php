<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Soru.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $soru = new Soru($db);

  // Blog post query
  $sonuc = $soru->sorulari_getir();
  // Get row count
  $satir_sayisi = $sonuc->rowCount();

  // Check if any posts
  if($satir_sayisi > 0) {
    // Post array
    $sorular_dizi = array();
    // $posts_arr['data'] = array();

    while($row = $sonuc->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $sorular_item = array(
        'id' => $id,
        'soru' => $soru,
        'oylayan_sayisi' => (int)$oylayan_sayisi,
        'created_at' => $created_at
      );

      // Push to "data"
      array_push($sorular_dizi, $sorular_item);
      // array_push($posts_arr['data'], $post_item);
    }

    // Turn to JSON & output
    echo json_encode($sorular_dizi);

  } else {
    // No Posts
    echo json_encode(
      array('message' => 'No Posts Found')
    );
  }
