<?php 


header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
  

  include_once '../../config/Database.php';
  include_once '../../models/Kullanici.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $kullanici = new Kullanici($db);

   // Get raw posted data
    $data = json_decode(file_get_contents("php://input"));
    $kullanici->adsoyad = $data->adsoyad;


  $sonuc = $kullanici->kullanici_bul();

  $satir_sayisi = $sonuc->rowCount();

  if($satir_sayisi > 0) {
    // Post array
    $kullanici_dizi = array();
    // $posts_arr['data'] = array();

    while($row = $sonuc->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $kullanici_item = array(
        'id' => $id,
        'adsoyad' => $adsoyad,
        'mail' => $mail,
        'sifre' => $sifre,
        'puan' => $puan,
        'created_at' => $created_at
      );

      // Push to "data"
      array_push($kullanici_dizi, $kullanici_item);
      // array_push($posts_arr['data'], $post_item);
    }

    // Turn to JSON & output
    echo json_encode($kullanici_dizi);

  } else {

    $kullanici_dizi = array();

    $kullanici_item = array(
        'id' => "boş",
        'adsoyad' => "boş",
        'mail' => "boş",
        'sifre' => "boş",
        'puan' => "boş",
        'created_at' => "boş"
      );
      array_push($kullanici_dizi, $kullanici_item);

      echo json_encode($kullanici_dizi);

  }
