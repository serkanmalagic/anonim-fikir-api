<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Kullanici.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate blog post object
$kullanici = new Kullanici($db);

 // Get raw posted data
 $data = json_decode(file_get_contents("php://input"));

 $kullanici->mail = $data->mail;
 $kullanici->sifre = $data->sifre;

//SINIFTAKİ BİLGİLER VERİTABANINDAN GETİRİLEREK BURADA SADECE JSON İLE DÖNDERİLMESİ KALMAKTADIR.
$kullanici->kullanici_giris();


if ($kullanici->mail == "boş") {
    $kullanici_arr = array(
        'cevap' => '0',
    );
} else {
    // Create array
    $kullanici_arr = array(
        'cevap' => '1',
        'id' => $kullanici->id,
        'adsoyad' => $kullanici->adsoyad,
        'mail' => $kullanici->mail,
        'sifre' => $kullanici->sifre,
        'puan' => $kullanici->puan,
        'created_at' => $kullanici->created_at
    );
}


// Make JSON
echo json_encode($kullanici_arr);
