<?php 

  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: POST');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../models/Kullanici.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $kullanici = new Kullanici($db);

  // Get raw posted data
  $data = json_decode(file_get_contents("php://input"));

  $kullanici->adsoyad = $data->adsoyad;
  $kullanici->mail = $data->mail;
  $kullanici->sifre = $data->sifre;
  $kullanici->puan = $data->puan;

  // Create post
  if($kullanici->kullanici_kayit()) {
    echo json_encode(
      array('cevap' => '1')
    );
  } else {
    echo json_encode(
        array('cevap' => '0')
    );
  }

