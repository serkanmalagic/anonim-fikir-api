<?php 

  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Kullanici.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $kullanici = new Kullanici($db);

  // Blog post query
  $sonuc = $kullanici->tum_kullanicilar();
  // Get row count
  $satir_sayisi = $sonuc->rowCount();

  // Check if any posts
  if($satir_sayisi > 0) {
    // Post array
    $kullanici_dizi = array();
    // $posts_arr['data'] = array();

    while($row = $sonuc->fetch(PDO::FETCH_ASSOC)) {
      extract($row);

      $kullanici_item = array(
        'id' => $id,
        'adsoyad' => $adsoyad,
        'mail' => $mail,
        'sifre' => $sifre,
        'puan' => $puan,
        'created_at' => $created_at
      );

      // Push to "data"
      array_push($kullanici_dizi, $kullanici_item);
      // array_push($posts_arr['data'], $post_item);
    }

    // Turn to JSON & output
    echo json_encode($kullanici_dizi);

  } else {
    // No Posts
    echo json_encode(
      array('message' => 'No Posts Found')
    );
  }
