<?php 

  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Kullanici.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $kullanici = new Kullanici($db);

  // Get ID
  $kullanici->mail = isset($_GET['mail']) ? $_GET['mail'] : die();

  // Get post
  //SINIFTAKİ BİLGİLER VERİTABANINDAN GETİRİLEREK BURADA SADECE JSON İLE DÖNDERİLMESİ KALMAKTADIR.
  $kullanici->kullanici_kontrol();

  if ($kullanici->mail == null){
    $kullanici_arr = array(
        'cevap' => '0',
    );
  }else{
        // Create array
        $kullanici_arr = array(
            'cevap' => '1',
            'id' => $kullanici->id,
            'adsoyad' => $kullanici->adsoyad,
            'mail' => $kullanici->mail,
            'sifre' => $kullanici->sifre,
            'puan' => $kullanici->puan,
            'created_at' => $kullanici->created_at
        );
  }
  

  // Make JSON
  echo json_encode($kullanici_arr);